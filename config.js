/*
 *  _____                    _                    _
 * |  __ \                  | |                  | |
 * | |  | |  ___   ___  ____| |  ___    __ _   __| |  ___  _ __
 * | |  | | / _ \ / _ \|_  /| | / _ \  / _` | / _` | / _ \| '__|
 * | |__| ||  __/|  __/ / / | || (_) || (_| || (_| ||  __/| |
 * |_____/  \___| \___|/___||_| \___/  \__,_| \__,_| \___||_|
 *
 *  Version 2.1.0
 *
 *  Maintained by ParadoxalManiak <https://www.reddit.com/user/ParadoxalManiak/>
 *  Original work by ZzMTV <https://boerse.to/members/zzmtv.3378614/>
 * */

var config = {
  // Port for the server
  serverPort: 1720,

  // Launch browser to server location at startup ?
  launchBrowser: true
};

module.exports = config;